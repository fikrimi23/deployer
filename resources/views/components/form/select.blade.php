<div class="input-field col s12">
    {!! Form::select($name, $options, $default, array_merge(['class' => ''], $attributes, ['data-placeholder' => $attributes['placeholder'] ?? ''])) !!}
    {{ Form::label($name, $label, ['class' => '']) }}
</div>
