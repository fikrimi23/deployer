<div class="input-field col s12">
    {{ Form::label($name, $label, ['class' => 'validate']) }}
    {{ Form::text($name, $value, array_merge([], $attributes)) }}
</div>
