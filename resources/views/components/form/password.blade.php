<div class="input-field col s12">
    {{ Form::label($name, $label, ['class' => 'validate']) }}
    {{ Form::password($name, array_merge([], $attributes)) }}
</div>
