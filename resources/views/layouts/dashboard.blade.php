<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title', "Dashboard")</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    @stack('css')
</head>
<body>
    @if (auth()->check())
        <nav>
            <div class="nav-wrapper indigo">
                <a href="{{ route('projects.index') }}" class="brand-logo" style="padding-left: 20px">Deployer</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    {!! Form::open(['url' => route('logout'), 'id' => 'form-logout']) !!}
                        <li><a onclick="$('#form-logout').submit()" type="submit">Logout</a></li>
                    {!! Form::close() !!}
                </ul>
            </div>
        </nav>
    @endif

    @yield('main-content')

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.1/js/materialize.min.js"></script>
    @stack('js')
</body>
</html>