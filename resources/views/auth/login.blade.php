@extends('layouts.dashboard')

@push('css')
<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    main {
        flex: 1 0 auto;
    }

    body {
        background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
        color: #e91e63;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
        border-bottom: 2px solid #e91e63;
        box-shadow: none;
    }
    input:-webkit-autofill {
        -webkit-box-shadow: 0 0 0 30px #f5f5f5 inset !important;
    }
</style>
@endpush

@section('main-content')
<body style="background-color: #eee">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="hidden toastMessage" data-content="{{ $error }}"></div>
        @endforeach
    @endif
    <div class="section"></div>
    <main>
        <center>
            <div class="section"></div>

            <h5 class="indigo-text">Please, login into your account</h5>
            <div class="section"></div>

            <div class="container">
                <div class="row">
                    <div class="col s12 m8 l6 offset-m4 offset-l3">
                        <div class="z-depth-1 grey lighten-4 row" style="padding: 32px 48px 0px 48px; border: 1px solid #EEE;">
                            {!! Form::open(['url' => route('login'), 'class' => 'col s12']) !!}
                                <div class='row'>
                                    <div class='col s12'>
                                    </div>
                                </div>

                                <div class='row'>
                                    {{ Form::mzText('email', 'Email') }}
                                </div>

                                <div class='row'>
                                    {{ Form::mzPassword('password', 'Password') }}
                                    {{-- <label style='float: right;'> --}}
                                        {{-- <a class='pink-text' href='#!'><b>Forgot Password?</b></a> --}}
                                    {{-- </label> --}}
                                </div>

                                <center>
                                    <div class='row'>
                                        {!! Form::button('Login', ['class' => 'col s12 btn btn-large waves-effect indigo', 'type' => 'submit']) !!}
                                        {{-- <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect indigo'>Login</button> --}}
                                    </div>
                                </center>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            {{-- <a href="#!">Create account</a> --}}
        </center>

        <div class="section"></div>
        <div class="section"></div>
    </main>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $(".toastMessage").each(function(index) {
                Materialize.toast($(this).data('content'), 4000, 'red')
            });
        });
    </script>
@endpush