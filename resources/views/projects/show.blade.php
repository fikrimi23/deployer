@extends('layouts.dashboard')

@push('css')
<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    main {
        flex: 1 0 auto;
    }

    body {
        background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
        color: #e91e63;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
        border-bottom: 2px solid #e91e63;
        box-shadow: none;
    }
    input:-webkit-autofill {
        -webkit-box-shadow: 0 0 0 30px #fafafa  inset !important;
    }
</style>
@endpush

@section('main-content')
<body style="background-color: #eee">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="hidden toastMessage" data-content="{{ $error }}"></div>
        @endforeach
    @endif
    <div class="section"></div>
    <main>
        <center>
            <div class="section">
                <h4>{{ $project->name }} <a href="{{ route('projects.edit', $project->id) }}" class="btn waves-effect waves-light red" style="padding:0 10px"><i class="material-icons" style="margin:0">edit</i></a></h4>
            </div>

            <div class="container-fluid">
                <div class="row">
                    <div class="col s12 m6 l6">
                        <div class="z-depth-1 grey lighten-5 row" style="padding: 32px 48px 32px 48px;">
                            <div class="row">
                                <div class="col s12">
                                    <h5 class="left-align">Details</h5>
                                </div>
                            </div>
                            <div class='row'>
                                {{ Form::mzText('name', 'Project Name', $project->name, ['disabled']) }}
                            </div>
                            <div class='row'>
                                {{ Form::mzText('branch', 'Branch', $project->branch, ['disabled']) }}
                            </div>
                            <div class='row'>
                                {{ Form::mzText('path', 'Path', $project->path, ['disabled']) }}
                            </div>
                            <div class='row'>
                                {{ Form::mzSelect('status', 'Status', ['0' => 'Inactive', '1' => 'Active'], $project->status, ['disabled']) }}
                            </div>
                            <div class='row'>
                                {{ Form::mzText('secret', 'Secret Token', $project->secret, ['disabled']) }}
                            </div>
                            <div class='row'>
                                {{ Form::mzText('pre_hook', 'Pre Hook Script', $project->pre_hook, ['disabled']) }}
                            </div>
                            <div class='row'>
                                {{ Form::mzText('post_hook', 'Post Hook Script', $project->post_hook, ['disabled']) }}
                            </div>
                        </div>
                    </div>
                    <div class="col s12 m6 l6">
                        <div class="z-depth-1 grey lighten-5 row" style="padding: 32px 48px 32px 48px;">
                            <div class="row">
                                <div class="col s12">
                                    <h5 class="left-align">Logs</h5>
                                </div>
                            </div>
                            <table class="bordered">
                                <thead>
                                    <tr>
                                        <th width="30%">Time</th>
                                        <th>Content</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($project->logs as $log)
                                    <tr>
                                        {{-- <div class="row"> --}}
                                            {{-- <div class="col s12"><b></div> --}}
                                        <td><b>{{ $log->created_at }}</b></td>
                                        <td> {!! $log->content !!}</td>
                                        {{-- </div> --}}
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>

    </main>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('select').material_select();
            $(".toastMessage").each(function(index) {
                Materialize.toast($(this).data('content'), 4000, 'red')
            });
        });
    </script>
@endpush