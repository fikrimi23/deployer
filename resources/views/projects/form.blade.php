@extends('layouts.dashboard')

@push('css')
<style>
    body {
        display: flex;
        min-height: 100vh;
        flex-direction: column;
    }

    main {
        flex: 1 0 auto;
    }

    body {
        background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
        color: #e91e63;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
        border-bottom: 2px solid #e91e63;
        box-shadow: none;
    }
    input:-webkit-autofill {
        -webkit-box-shadow: 0 0 0 30px #fafafa  inset !important;
    }
</style>
@endpush

@section('main-content')
@php
    $isEdit = isset($project);
    if ($isEdit) {
        $formUrl = route('projects.update', $project->id);
        $formMethod = "PATCH";
    } else {
        $formUrl = route('projects.store');
        $formMethod = "POST";
    }
@endphp
<body style="background-color: #eee">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="hidden toastMessage" data-content="{{ $error }}"></div>
        @endforeach
    @endif
    <div class="section"></div>
    <main>
        <center>
            <div class="section">
                <h4>{{ $isEdit ? "EDIT" : "CREATE" }} PROJECT</h4>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col s12 m10 l8 offset-m4 offset-l2">
                        <div class="z-depth-1 grey lighten-5 row" style="padding: 32px 48px 0px 48px;">
                            {!! Form::model($project ?? null, ['url' => $formUrl, 'class' => 'col s12', 'method' => $formMethod]) !!}
                                <div class='row'>
                                    {{ Form::mzText('name', 'Project Name') }}
                                </div>
                                <div class='row'>
                                    {{ Form::mzText('branch', 'Branch') }}
                                </div>
                                <div class='row'>
                                    {{ Form::mzText('namespace', 'Project Namespace (user/project)') }}
                                </div>
                                <div class='row'>
                                    {{ Form::mzText('path', 'Path') }}
                                </div>
                                <div class='row'>
                                    {{ Form::mzSelect('status', 'Status', ['0' => 'Inactive', '1' => 'Active']) }}
                                </div>
                                <div class='row'>
                                    {{ Form::mzText('secret', 'Secret Token') }}
                                </div>
                                <div class='row'>
                                    {{ Form::mzText('pre_hook', 'Pre Hook Script') }}
                                </div>
                                <div class='row'>
                                    {{ Form::mzText('post_hook', 'Post Hook Script') }}
                                </div>

                                <center>
                                    <div class='row'>
                                        {!! Form::button($isEdit ? "SUBMIT" : "CREATE", ['class' => 'col s12 btn btn-large waves-effect indigo', 'type' => 'submit']) !!}
                                    </div>
                                </center>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </center>

    </main>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $('select').material_select();
            $(".toastMessage").each(function(index) {
                Materialize.toast($(this).data('content'), 4000, 'red')
            });
        });
    </script>
@endpush