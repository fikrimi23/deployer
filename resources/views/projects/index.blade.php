@extends('layouts.dashboard')

@section('main-content')
@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="hidden toastMessage" data-content="{{ $error }}"></div>
    @endforeach
@endif

<div class="container main-content">
    <div class="row">
        <div class="col s12">
            <div style="float:right; margin-bottom: 20px;">
                <a href="{{ route('projects.create') }}" title="Create Project" class="btn indigo darken-2 waves-effect waves-light"><i class="material-icons right">add_circle_outline</i> Create</a>
            </div>
            <table class="bordered">
                <thead>
                    <tr>
                         <th width="30%">Name</th>
                         <th width="40%">Last Log</th>
                         <th width="10%">Status</th>
                         <th width="10%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($projects as $project)
                        <tr>
                            <td>{{ $project->name }}</td>
                            @if (isset($project->last_log))
                                <td>{!! "<b>" . $project->last_log->updated_at . "</b> - " . $project->last_log->content !!}</td>
                            @else
                                <td>Nothing to show here</td>
                            @endif
                            <td>{{ $project->status_name }}</td>
                            <td><a href="{{ route('projects.show', $project->id) }}" class="btn red waves-effect waves-light"><i class="material-icons left" style="margin:0">info_outline</i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@push('js')
    <script>
        $(document).ready(function() {
            $(".toastMessage").each(function(index) {
                Materialize.toast($(this).data('content'), 4000, 'red')
            });
        });
    </script>
@endpush