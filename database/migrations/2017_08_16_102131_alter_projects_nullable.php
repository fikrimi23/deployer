<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProjectsNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('projects')) {
            Schema::table('projects', function($table) {
                $table->string('secret')->nullable()->change();
                $table->string('pre_hook')->nullable()->change();
                $table->string('post_hook')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('projects')) {
            Schema::table('projects', function($table) {
                $table->string('secret')->change();
                $table->string('pre_hook')->change();
                $table->string('post_hook')->change();
            });
        }
    }
}
