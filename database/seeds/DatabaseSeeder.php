<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::beginTransaction();
        try {
            $this->call(UsersTableSeeder::class);
            \DB::commit();
        } catch (Exception $e) {
            print_r($e->getMessage());
            \DB::rollback();
        }
    }
}
