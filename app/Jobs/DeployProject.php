<?php

namespace App\Jobs;

use App\Project;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class DeployProject implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $project;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Project $project)
    {
        $this->project = $project;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $log = new \App\Log();

        $project = $this->project;
        $params = [
            'branch' => $project->branch,
            'path' => $project->path,
        ];

        $param_string = '';
        foreach ($params as $key => $param) {
            $param_string .= " --".$key."='".$param."'";
        }
        $commands = [
            "git_checkout" => "git checkout ".$params['branch'],
            "git_pull" => "git pull origin ".$params['branch'],
            "artisan_migrate" => "php artisan migrate",
            // "composer_install" => "/usr/bin/composer install",
        ];
        $directory = $params['path'];

        $log->content = "";
        foreach ($commands as $key => $command) {
            $process = new Process($command);
            $process->setTimeout(3600);
            $process->setIdleTimeout(300);
            $process->setWorkingDirectory($directory);

            try {
                $process->mustRun();
                $log->content .= "Process ".$key." executed successfully\n";
            } catch (ProcessFailedException $e) {
                $log->content .= "Process ".$key." error:\n";
                // $log->status = "0";
                $log->content .= $e->getMessage();
            }
        }
        $log->status = "1";

        $log->content = preg_replace("/(\[3[0-9]m)/", "", str_replace("\e",'', nl2br(($log->content))));
        $project->logs()->save($log);
    }
}
