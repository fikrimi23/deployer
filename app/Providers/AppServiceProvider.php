<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        \Form::component('mzText', 'components.form.text', ['name', 'label', 'value' => null, 'attributes' => []]);
        \Form::component('mzPassword', 'components.form.password', ['name', 'label', 'attributes' => []]);
        \Form::component('mzSelect', 'components.form.select', ['name', 'label', 'options' => [], 'default' => null, 'attributes' => []]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
