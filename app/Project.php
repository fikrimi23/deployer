<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $guarded = [];

    protected $appends = ['status_name'];

    protected $status_codename = [
        0 => 'Inactive',
        1 => 'Active',
    ];

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function getStatus($status)
    {
        return $this->status_codename[$status];
    }

    public function getStatusNameAttribute()
    {
        return $this->getStatus($this->status);
    }

    public function getLastLogAttribute()
    {
        return $this->logs->sortByDesc('updated_at')->first();
    }

    public function logs()
    {
        return $this->hasMany(Log::class)->orderBy('updated_at', 'desc');
    }
}
