<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use DB;
use App\Jobs\DeployProject;

class WebhookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hook_data = collect(json_decode(file_get_contents("php://input")));
        $log = new \App\Log();

        $projects = \App\Project::where('namespace', ($hook_data['project'])->path_with_namespace)->get();
        foreach ($projects as $project) {
            if ($project === null) {
                $log->status = "0";
                $log->content = "Repository Not Found";
                $project->logs()->save($log);
                return response("Failed", 200);
            }

            dispatch((new DeployProject($project))->delay(\Carbon\Carbon::now()->addSeconds(5)));
        }

        return response("Success", 200);
    }
}
