@servers(['localhost' => '127.0.0.1'])

@task('deploy', ['on' => 'localhost'])
    cd {{ $path }}
    composer install
    git pull origin {{ $branch }}
    php artisan migrate
@endtask